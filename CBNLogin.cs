﻿using System;
using System.Collections.Generic;
using System.Text;
using WeekEndTask.EventHandlers;

namespace WeekEndTask
{
    class CBNLogin
    {
        //Implement a Login System for Central Bank.The conditions are as follow:
        //Users are able to log in, and check their BVN, or Enroll for BVN.
        //James, Bill, and Jane are banned from using any banking system.So, when any of
        //them tries to log in, the application raised an event and fire alarm as well as sends an email to Central Bank.

        public static void Run()
        {

            var validator = new LoginValidator();

            // listen for  userloggedin events from the loginValidator class
            validator.UserLoggedIn += Greeting.HandleUserLoggedIn;
            validator.UserLoggedIn +=   FireAlarm.HandleUserLoggedIn;
            validator.UserLoggedIn += Email.HandleUserLoggedin;


            while (true)
            {
                Console.WriteLine("Enter your username");
                var userName = Console.ReadLine();
                validator.Validate(userName);
                Console.WriteLine();
            }


        }
       
       
        



    }
}
