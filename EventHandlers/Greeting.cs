﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeekEndTask.EventHandlers
{

    public class Greeting
    {
        // This is an event handler for the loginValidator.Userloggedin event,this
        public static void HandleUserLoggedIn(object sender, LoginEventArgs loginEvent)
        {

            if (loginEvent.Success)
            {
                Console.WriteLine($"Hi {loginEvent.UserName}, Welcome to CBN");
            }
            else
            {
                Console.WriteLine($"Dear {loginEvent.UserName}, You have been banned!");
            }

        }
    }
}
