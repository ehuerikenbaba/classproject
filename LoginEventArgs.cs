﻿using System;

namespace WeekEndTask
{
   public  class LoginEventArgs : EventArgs
    {
        public bool Success { get; set; }
        public string UserName { get; set; }
    }

}
