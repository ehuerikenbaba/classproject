﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeekEndTask
{
    class CustomKeyboard
    {
        // Given a non-empty string consisting only of special chars(!, @, # etc.), return a number (as a string)
        //where each digit corresponds to a given special char on the custom keyboard ( 1→ ), 2 → (, 3 → *, 4→&, 5 →^, 6 →%, 7 →$, 8 →#, 9 →@, 0 →!  ).
        //Expected input and output
        //"())(" → "2112"
        //"*$(#&" → "3284"
        //"!!!!!!!!!!" → "0000000000"
        //The Questions Above Should be Implemented using LINQ in both QuerySyntax and Method Syntax

        public static string Run(string inputString)
        {
            // 1. store values map in a data type (tuples array)
            var valuesMap = new[] { Tuple.Create(')','1') , Tuple.Create('(','2'),
                Tuple.Create('*','3'), Tuple.Create('&','4'), Tuple.Create('^','5'),
                Tuple.Create('%','6'), Tuple.Create('$','7'), Tuple.Create('#','8'),
                Tuple.Create('@','9'), Tuple.Create('!','0')

            };

            // 3. Declare and initialize output string as empty string
            StringBuilder output = new StringBuilder();


            // 4. for each Character  in the input string
            foreach (var letter in inputString)
            {
                //  a. search the valus map for the tuple whose key matches that character
                var value = valuesMap.Where((v) => v.Item1 == letter).FirstOrDefault();

                //  b. if found in step 'a', append a value that tuple to the output string
                output.Append(value.Item2);
            }

            // 5. return output string

            return output.ToString();


        }
    }
}
