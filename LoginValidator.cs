﻿using System;
using System.Linq;

namespace WeekEndTask
{
    class LoginValidator
    {
        // user loggedin event
        public event EventHandler<LoginEventArgs> UserLoggedIn;

        public void Validate(string name)
        { 
            // store list of banned users
            var bannedUsers = new[] { "james", "bill", "jane" };

            

            bool loginStatus;

            // check if the current  user name exist in the banned  users list
            if (bannedUsers.Contains(name.ToLower()))
            {
                loginStatus = false;
            }
            else
            {
                loginStatus = true;
            }
            
            // raise a login event passing user name and logging status 
            var args = new LoginEventArgs() { UserName = name, Success = loginStatus };
            this.OnUserLoggedIn(args);

        }

        public void OnUserLoggedIn(LoginEventArgs eventArgs)
        {

            var handler = UserLoggedIn;

            // check if a handler has been registered and call the handler passing the event 
            if (handler != null)
            {
                handler(this, eventArgs);
            }

        }
    }




}
